if status is-interactive
    alias cd=z
    zoxide init fish | source
    alias dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"
    function dotfiles --wraps git --description 'alias my .dotfiles git command'
        /usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME $argv
    end
end

alias sway="sway --unsupported-gpu"
