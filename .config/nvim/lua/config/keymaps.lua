-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here
local wk = require("which-key")
wk.add({
  { "<leader>C", group = "CMakeTools" },
})
vim.keymap.set("n", "<leader>Ck", "<cmd>CMakeSelectKit<cr>", { desc = "Select CMake Kit" })
vim.keymap.set("n", "<leader>Cg", "<cmd>CMakeGenerate<cr>", { desc = "CMake Generate" })
vim.keymap.set("n", "<leader>Cb", "<cmd>CMakeBuild<cr>", { desc = "CMake build" })
