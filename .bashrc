#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --color=auto'
PS1='[\u@\h \W]\$ '
setxkbmap se

alias dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"
export HISTSIZE=10000
export HISTFILESIZE=10000
HISTCONTROL=ignorespace


_streamlink () {
  streamlink --plugin-dir ~/.streamlink $1 best 1>/dev/null &
}
