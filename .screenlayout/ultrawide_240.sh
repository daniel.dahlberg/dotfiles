#!/bin/sh

MONITOR_NAME=$(xrandr --listmonitors | awk 'NR==2 {print $4}')

if [ -z "$MONITOR_NAME" ]; then
  echo "No monitor found, exiting."
  exit 1
fi

echo "$MONITOR_NAME"

xrandr --output $MONITOR_NAME --primary --mode 5120x1440 --rate 240 --pos 0x0 --rotate normal
