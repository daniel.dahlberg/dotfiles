#!/bin/sh
xrandr --output DP-0 --off --output DP-1 --off --output HDMI-0 --mode 1920x1080 --pos 4480x0 --rotate left --output DP-2 --primary --mode 1920x1080 --pos 1920x180 --rate 144 --rotate normal --output DP-3 --off --output HDMI-1 --mode 1920x1080 --pos 0x420 --rotate normal --output DP-4 --off --output DP-5 --off
